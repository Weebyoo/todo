<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Checkbox extends Migration
{
    Schema::table('users', function($table) {
        $table->integer('paid');
    });
}
    public function down()
{
    Schema::table('users', function($table) {
        $table->dropColumn('paid');
    });
}
    public function down()
    {
        //
    }
}
