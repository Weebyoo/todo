<x-app-layout>
<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Dashboard') }}
    </h2>
</x-slot>

<div class="py-6">
    <div class="max-w-17xl mx-auto sm:px-6 lg:px-6">
        <div class="bg-white overflow-hidden shadow-xl rounded mb-4">
            <div class="flex">
                <div class="flex-auto text-2xl mb-4 py-2 px-2">Tasks List</div>
                
                <div class="flex-auto text-right mt-3">
                    <a href="/task" class="bg-blue-500 hover:bg-blue-700 text-gray font-bold py-2 px-4 rounded">   Add new Task</a>
                </div>
            </div>
            <table class="w-full text-md rounded mb-4">
                <thead>
                <tr class="border-b">
                    <th class="text-left p-13 px-15">Task</th>
                    <th class="text-left p-13 px-15">Actions</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach(auth()->user()->tasks as $task)
                    <tr class="border-b hover:bg-orange-100">
                        <td class="p-6 px-6">
                            {{$task->description}}
                        </td>
                        <td class="p-5 px-5">
                            
                            <a href="/task/{{$task->id}}" name="edit" class="mr-3 text-sm bg-blue-500 hover:bg-blue-700 text-blue py-1 px-12 rounded focus:outline-none focus:shadow-outline">Edit</a>
                            <form action="/task/{{$task->id}}" class="inline-block">
                                <button type="submit" name="delete" formmethod="POST" class="text-sm bg-red-500 hover:bg-red-700 text-red py-2 px-5 rounded focus:outline-none focus:shadow-outline">Delete</button>
                                {{ csrf_field() }}
                            </form>
                            <div class="form-group">
                              <input type="checkbox" class="" id="onloan" value="0" name="tdone">task done
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            
        </div>
    </div>
</div>
</x-app-layout>
